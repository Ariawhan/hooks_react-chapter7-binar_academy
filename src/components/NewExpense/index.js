import React, { useState } from "react";
import ExpenseForm from "./ExpenseForm";
import "./style.css";

const NewExpense = (props) => {
  const [isAddNewData, setIsNewData] = useState(false);

  const saveExpenseDataHandler = (enteredExpenseData) => {
    const expenseData = {
      ...enteredExpenseData,
      id: Math.random().toString(),
    };
    console.log(expenseData);
    props.onAddExpense(expenseData);
  };

  const startAddNewData = () => {
    setIsNewData(true);
  };

  const cancelAddNewData = () => {
    setIsNewData(false);
  };

  return (
    <div className="new-expense">
      {!isAddNewData && (
        <button onClick={startAddNewData}>Add New Expense</button>
      )}
      {isAddNewData && (
        <ExpenseForm
          onSaveExpenseData={saveExpenseDataHandler}
          handleCancel={cancelAddNewData}
        />
      )}
    </div>
  );
};

export default NewExpense;
