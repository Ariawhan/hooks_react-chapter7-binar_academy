import "./style.css";

import React, { Component } from "react";

class ExpenseFilter extends Component {
  state = {};
  handlerChange = (event) => {
    this.props.onSaveExpenseFilter(event.target.value);
  };
  render() {
    return (
      <div className="expenses-filter">
        <div className="expenses-filter_control">
          <label>Filter By Years</label>
          <select selected={this.props.selected} onChange={this.handlerChange}>
            <option value={"all"}>All</option>
            <option value={"2022"}>2022</option>
            <option value={"2021"}>2021</option>
            <option value={"2020"}>2020</option>
            <option value={"2019"}>2019</option>
          </select>
        </div>
      </div>
    );
  }
}

export default ExpenseFilter;
