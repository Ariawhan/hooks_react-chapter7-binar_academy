import React, { useState } from "react";
import ExpenseFilter from "./ExpenseFilter";
import ExpensesChart from "./ExpensesChart";
import ExpanseList from "./ExpanseList";
import Card from "../UI/Card";
import "./Expenses.css";

const Expenses = (props) => {
  const [filter, setFilter] = useState("all");
  const onSaveExpenseFilter = (filter) => {
    setFilter(filter);
  };

  let filterItmes = props.items;
  if (filter !== "all") {
    filterItmes = props.items.filter((item) => {
      return item.date.getFullYear() === parseInt(filter);
    });
  }

  return (
    <Card className="expenses">
      <ExpenseFilter
        selected={filter}
        onSaveExpenseFilter={onSaveExpenseFilter}
      ></ExpenseFilter>
      <ExpensesChart expenses={filterItmes} />
      <ExpanseList filterItmes={filterItmes}></ExpanseList>
    </Card>
  );
};

export default Expenses;
