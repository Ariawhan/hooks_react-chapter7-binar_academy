import React, { Component } from "react";
import ExpenseItem from "../ExpenseItem";
import "./style.css";

class ExpanseList extends Component {
  render() {
    let expensesContent = (
      <p className="expenses-list__fallback">No Expenses found</p>
    );
    // consolel.log("list", this.state.items);
    if (this.props.filterItmes.length === 0) {
      return expensesContent;
    }
    return this.props.filterItmes.map((items) => {
      return (
        <ExpenseItem
          title={items.title}
          amount={items.amount}
          date={items.date}
        />
      );
    });
  }
}

export default ExpanseList;
